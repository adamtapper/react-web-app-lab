import React from 'react';
import axios from 'axios';
import CalculatorService from '../CalculatorService';
jest.mock('axios');

const axiosMock = axios.post as jest.Mock;

describe('Unit Tests', () => {
    beforeEach(() => {
        axiosMock.mockReset();
    });

    test('calculate_successfulCalculation_calculationResultReturned', async () => {
        axiosMock.mockReturnValue(Promise.resolve({status: 200, data: { Result: 12}}));

        const response = await CalculatorService.calculate({FirstOperand: 4, SecondOperand: 3, Operator: 'x'});

        expect(response).toEqual({Result: 12});
    });

    test('calculate_failedCalculation_zeroReturnedAsResult', async () => {
        axiosMock.mockRejectedValue(Promise.resolve({status: 500}));

        const response = await CalculatorService.calculate({FirstOperand: 4, SecondOperand: 3, Operator: 'x'});

        expect(response).toEqual({Result: 0});
    });
});