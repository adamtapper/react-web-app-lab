import axios from "axios";
import CalculationModel from "../models/CalculationModel";

class CalculatorService {
    calculate = async (model: CalculationModel) => {
        try {    
            const response = await axios.post('/calculator', model);
            return response.data;
        } catch (error) {
            return { Result: 0 };
        }
    }
}

export default new CalculatorService();