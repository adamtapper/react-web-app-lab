import React from 'react';

interface ButtonProps {
  name: string
  orange?: boolean
  wide?: boolean
  handleButtonPress: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, name: string) => void
}

const Button = ({name, orange, wide, handleButtonPress}: ButtonProps) => {
    const formatClassName = () => {
        return `component-button${orange ? " orange" : ""}${wide ? " wide" : ""}`;
    }

    return (
        <div className={formatClassName()}>
          <button data-testid='button' onClick={event => handleButtonPress(event, name)}>{name}</button>
        </div>
    );
};

export default Button;