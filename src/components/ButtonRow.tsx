import React from 'react';
import Button from './Button';

type CalculatorButton = {
    orange?: boolean
    wide?: boolean
    name: string
}

interface ButtonRowProps {
  buttons: CalculatorButton[]
  handleButtonPress: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, name: string) => void
}

const ButtonRow = ({buttons, handleButtonPress}: ButtonRowProps) => {
    const mapButtons = () => {
        return buttons.map((button, i) => {
            return <Button
                key={i}
                name={button.name}
                orange={button.orange}
                wide={button.wide}
                handleButtonPress={handleButtonPress}
            />
        });
    };

    return (
        <div>
          {mapButtons()}
        </div>
    );
};

export default ButtonRow;