import React from 'react';
import { render, screen, fireEvent } from "@testing-library/react";
import Button from '../Button';

// Key Concept: Test Grouping
describe('Unit Tests', () => {
    test('render_buttonName_buttonNameDisplayed', () => {
        render(<Button orange={true} wide={true} handleButtonPress={jest.fn()} name={'test'} />);

        // Key Concept: screen in React Testing Library
        expect(screen.getByText('test')).toBeTruthy();
    });

    test('handleButtonPress_clickButton_handleFunctionCalledWithButtonName', () => {
        // Key Concept: Mock Function
        const handleFn = jest.fn();
        render(<Button orange={true} wide={true} handleButtonPress={handleFn} name={'test'} />);

        // Key Concept: fireEvent from React testing library
        fireEvent.click(screen.getByText('test'));

        expect(handleFn).toHaveBeenCalledTimes(1);
        // Key Concept: Pulling data from mock
        expect(handleFn.mock.calls[0][1]).toBe('test');
    });

    // Key Concept: Parameterized Tests
    test.each([
        [false, false, 'component-button'],
        [true, false, 'component-button orange'],
        [false, true, 'component-button wide'],
        [true, true, 'component-button orange wide']
    ])('formatClassName_orange%sAndWide%s_classNameIs%s', (orange, wide, className) => { // Key Concept: Dynamic Test Naming
        render(<Button orange={orange} wide={wide} handleButtonPress={jest.fn()} name={'test'} />);

        // Key Concept: closest in React Testing Library
        const container = screen.getByText('test').closest('div') as HTMLElement;

        expect(container.className).toBe(className);
    });
});