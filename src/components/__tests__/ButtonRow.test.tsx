import React from 'react';
import { render, screen, fireEvent } from "@testing-library/react";
import ButtonRow from '../ButtonRow';

describe('Unit Tests', () => {
    // Key Concept: Test Naming
    // Task: Implement test below
    test('mapButtons_buttonNameArray_buttonDisplayedForEachName', () => {

    });
});

describe('Component Tests', () => {
    // Key Concept: Overlap between unit and component tests
    // Task: Implement parameterized test for class name combinations
    test.each([

    ])('formatClass_orange%sAndWide%s_classNameIs%s', (orange, wide, className) => {

    });

    // Task: Implement test below
    test('handleButtonPress_clickButton_handleFunctionCalled', () => {

    });
});