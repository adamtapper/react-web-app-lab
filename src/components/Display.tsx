import React from 'react';

interface DisplayProps {
    value: string
}

const Display = ({value}: DisplayProps) => {
    return (
        <div className='component-display'>
            <div data-testid='display'>{value}</div>
        </div>
    );
}

export default Display;
