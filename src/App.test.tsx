import React from 'react';
import axios from 'axios';
import { render, screen, fireEvent, within, waitFor } from '@testing-library/react';
import App from './App';
jest.mock('axios');

const axiosMock = axios.post as jest.Mock;

describe('Component Tests', () => {
  let display: HTMLElement;
  beforeEach(() => {
    render(<App />);
    display = screen.getByTestId('display').closest('.component-display') as HTMLElement;
  });

  const enterNumber = (digits: string) => {
    digits.split('').forEach(digit => {
      if (digit !== '-') {
        const elements = screen.getAllByText(digit);
        if (elements.length > 1) {
          fireEvent.click(elements[1]);
        } else {
            fireEvent.click(elements[0]);
        }
      }
    });

    if (digits.includes('-')) {
      fireEvent.click(screen.getByText('+/-'));
    }
  };

  // Key Concept: Nested grouping - these tests focus on the display prior to the calculation
  describe('Display', () => {
    test('render_noInput_zeroDisplayed', () => {
      // Key Concept: within from React testing library
      expect(within(display).getByText('0')).toBeTruthy();
    });

    // Task: Implement tests for inputDecimal method
    // Task: Implement tests for resetState method
    // Task: Implement tests for the percentage operand in the updateOperand method
    // Task: Implement tests for the integer operands in the updateOperand method
    // Task: Implement tests for the sign change operand in the updateOperand method
  });

  describe('Calculation', () => {
    afterEach(() => {
      // Key Concept: Clears any fake implementations but leaves the method as a mock object
      axiosMock.mockReset();
    });

    test.each([
      ['+'],
      ['-'],
      ['x'],
      ['÷']
    ])('handleoperator_%sPressedMultipleTimes_correctOperationSentToService', async (operator) => {
      enterNumber('3');

      fireEvent.click(screen.getByText(operator));
      fireEvent.click(screen.getByText(operator));

      enterNumber('4');
      fireEvent.click(screen.getByText('='));

      // Key Concept: Asynchronous code handling in React testing library
      await waitFor(() => expect(axiosMock).toHaveBeenCalledTimes(1));

      // Key Concept: toHaveBeenCalledWith vs .mock.calls
      expect(axiosMock).toHaveBeenCalledWith(
        '/calculator',
        {
          'firstOperand': 3,
          'operator': operator,
          'secondOperand': 4
        }
      );
    });

    // Task: Implement tests that apply a percentage before entering the operator then executing the calculation
    // Task: Implement tests that apply a percentage after entering the operator then executing the calculation
    // Task: Implement any other tests missing from the calculation describe block
  });
});