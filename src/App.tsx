import React, { useState } from 'react';
import CalculatorService from './services/CalculatorService';
import Display from './components/Display';
import ButtonPanel from './components/ButtonPanel';
import './App.css';

const App = () => {
  const digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const validOperands = [...digits, '%', '+/-', 'AC', '.'];
  const validOperators = ['+', '-', 'x', '÷'];
  const [display, setDisplay] = useState('0');
  const [firstOperand, setFirstOperand] = useState<number|null>(null);
  const [secondOperandEntered, setSecondOperandEntered] = useState<boolean>(false);
  const [operator, setOperator] = useState<string|null>(null);

  const executeCalculation = async () => {
    if (firstOperand !== null && operator !== null) {
      const response = await CalculatorService.calculate({
        firstOperand: firstOperand,
        secondOperand: Number(display),
        operator: operator
      });

      setDisplay(response.result);
      setOperator(null);
    }
  };

  const handleClick = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, buttonName: string) => {
    if (validOperands.includes(buttonName)) {
      updateOperand(buttonName);
    } else if (validOperators.includes(buttonName)) {
      handleOperator(buttonName);
    } else {
      await executeCalculation();
    }
  };

  const inputDecimal = (dot: string) => {
    if (!display.includes(dot) || secondOperandEntered) {
      if (secondOperandEntered) {
        setDisplay('0.')
        setSecondOperandEntered(false);
      } else {
        setDisplay(display + dot);
      } 
    }
  };

  const updateOperand = (buttonName: string) => {
    if (digits.includes(buttonName)) {
      if (display === '0') {
        setDisplay(buttonName);
      } else if (secondOperandEntered) {
        setDisplay(buttonName);
        setSecondOperandEntered(false);
      } else {
        setDisplay(display + buttonName);
      }
    } else if (buttonName === '.') {
      inputDecimal(buttonName);
    } else if (buttonName === '%') {
      setDisplay(`${Number(display) / 100}`);
    } else if (buttonName === '+/-') {
      setDisplay(`${-1 * Number(display)}`);
    } else {
      resetState();
    }
  }

  const handleOperator = (buttonName: string) => {
    setFirstOperand(Number(display));
    setOperator(buttonName);
    setSecondOperandEntered(true);
  };

  const resetState = () => {
    setDisplay('0');
    setFirstOperand(null);
    setSecondOperandEntered(false);
    setOperator(null);
  };

  return (
    <div className='component-app'>
        <Display value={display} />
        <ButtonPanel handleButtonPress={handleClick} />
    </div>
  );
}

export default App;
