# Playwright TypeScript Jest

Playwright is a web-based test automation library that tests against the underlying engine for the most popular browsers: Chromium for Chrome and Edge, Webkit for Safari and Gecko for Firefox. Test scripts can be written in JavaScript, Python, C# and Go. Playwright leverages the DevTools protocol to write powerful, stable automated tests.

## Globals
Globals can be accessed directly in any test by simply referencing the variable.
| Argument  | Description                  |
| --------- | ---------------------------- |
| Context   | Browser session              |
| Page      | Browser tab running the test |
| Device    | Mobile device being emulated |
| Browser   | The web browser object       |

## Command Line Arguments
Command line arguments can be passed when running the tests.

* Example: ```npm run test -- --engine=gecko --device=Galaxy-S5```
    * Notes
        * There must be two dashes between the command and the arguments to tell node where the command line arguments are
        * Any mobile device names must use dashes instead of spaces

| Argument  | Description                      | Options                 | Default       |
| --------- | -------------------------------- | ----------------------- | ------------- |
| Engine    | Browser engine                   | chromium, webkit, gecko | chromium      |
| Device    | Mobile device selected           | [devices](https://github.com/microsoft/playwright/blob/17e953c2d8bd19ace20059ffaaa85f3f23cfb19d/src/server/deviceDescriptors.js)              | none selected |
| Headed    | Option to display the browser    | true, false             | false         |
| Incognito | Whether web session is incognito | true, false             | false         |

## Page Components
* When modeling pages, page components should be used rather than heavy page objects.
* A page component is a section of the overall web page that performs a specific task.
    * Examples: navigation bar, search box, input form, etc.
        * Code example: [MenuBar](demo/components/MenuBar.ts)

## Key Playwright Concepts
1. [Auto-Waiting](https://playwright.dev/docs/actionability/) - Certain actions auto-wait for the element to be visible and actionable.
2. [Browser Contexts](https://playwright.dev/docs/api/class-browsercontext/) - BrowserContexts provide a way to operate multiple independent browser sessions.
3. [Element Selectors](https://playwright.dev/docs/selectors/) - Playwright can search for elements using CSS selectors, XPath selectors, HTML attributes and even text content.
4. [Network Interception](https://playwright.dev/docs/network) - Playwright provides APIs to monitor and modify network traffic, both HTTP and HTTPS.
5. [Permissions](https://playwright.dev/docs/emulation#permissions) - Playwright allows values to be set for certain permissions 