import { Page, Browser, BrowserContext } from "playwright";

declare global {
  const browser: Browser
  const context: BrowserContext
  const page: Page
  const device: string
}