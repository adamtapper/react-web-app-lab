const JSDOMEnvironment = require('jest-environment-jsdom');
const playwright = require('playwright');
const DeviceUtility = require('./device-utility.ts');

class PlaywrightEnvironment extends JSDOMEnvironment {
    constructor(config) {
        super(config);
    }

    getCommandLineArg(argument) {
        let value;
        process.argv.forEach(arg => {
          if (arg.split('=')[0] === `--${argument}`) {
            value = arg.split('=')[1];
          }
        });
        return value;
    }

    async setup() {
        await super.setup();
        this.global.device = this.getCommandLineArg('device');

        const headed = this.getCommandLineArg('headed') === 'true';
        const incognito = this.getCommandLineArg('incognito') === 'true';

        switch (this.getCommandLineArg('engine')) {
            case 'gecko':
                this.global.browser = await playwright.firefox.launch({headless: !headed});
                break;
            case 'webkit':
                this.global.browser = await playwright.webkit.launch({headless: !headed});
                break;
            default:
                this.global.browser =  await playwright.chromium.launch({headless: !headed});
        }

        if (this.global.device) { // check if session is using mobile web
            this.global.context = await this.global.browser.newContext({
                ...DeviceUtility.getDevice(this.global.device.replace(/-/g, ' ')),
            });
            this.global.page = await this.global.context.newPage();
        } else { // session is web browser
            if (incognito) { // check if web session should be incognito
                this.global.context = await this.global.browser.newContext();
                this.global.page = await this.global.context.newPage();
            } else {
                this.global.page = await this.global.browser.newPage();
            }
        }
    }

    async teardown() {
        await this.global.browser.close();
        await super.teardown();
    }
}

module.exports = PlaywrightEnvironment