const playwright = require('playwright');

class DeviceUtility {
    static getDevice(name) {
        const devices = playwright.devices;
        return devices[name];
    }
}

module.exports = DeviceUtility;