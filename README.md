# Installing Dependencies

* ```npm install``` or ```yarn install```

# Running Tests
* ```npm test``` - runs all tests once
* ```npm run test:watch``` - runs affected tests every time you save a file

# Starting Application
* ```npm run start``` - runs the application at http://localhost:3000